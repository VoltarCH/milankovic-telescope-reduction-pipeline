# Milankovic Telescope reduction pipeline

This reduction pipeline was written for optical data taken with the [Milankovic 1.4 meter telescope](http://articles.adsabs.harvard.edu/pdf/2018POBeo..98..333S) located at the top of the Vidojevica mountain, Serbia.
It includes calibration (dark, bias, flat fielding) and most importantly, a custom approach to estimate the sky background from the dithered scientific images themselves.
For this purpose, we mask all astronomical objects in the image such that we only have the sky and combine those background image to a background model.

The scientific purpose of the pipeline is published in Müller, Vudragovic, Bilek et al.

Please feel free to use the code as inspiration or straight forward to reduce your own set of data.
If you use our code, please be so kind and cite our paper.


## Pakages needed

You will need some standard packages like:
* numpy
* scipy
* astropy
* matplotlib
* [sep](https://sep.readthedocs.io/en/v1.0.x/)
