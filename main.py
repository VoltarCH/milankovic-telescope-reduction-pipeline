import sys
import os
import glob
from astropy.io import fits
from astropy.wcs import WCS
from scipy.ndimage import gaussian_filter
import numpy as np
import matplotlib.pyplot as plt
import sep

def source_detection(image, thresh=3, minarea=5,verbose=False):
    if verbose:
        print("run sep")

    # run source extractor
    image_c = image[::-1]  # reverse order of array such that the image is displayed correctly

    # we need to bring the array into a C Contiguous convention, I
    # this line make source extractor getting to work with the data file.
    image_c = image.byteswap().newbyteorder()
    bkg = sep.Background(image_c,maskthresh=thresh,bw=200)

    data_sub = image_c - bkg

    #this line starts Source Extractor to find the objects. We are mostly interested in the segmentation map
    objects, segmap = sep.extract(data_sub, thresh=thresh, err=bkg.globalrms, minarea=minarea,segmentation_map=True)
    if verbose:
        print("finished sep")

    return objects, data_sub, bkg, bkg.globalrms, segmap


def get_background(data,thresh=1,bw=64,bh=64,mask=None):
    image_c = data.byteswap().newbyteorder()
    return sep.Background(image_c,maskthresh=thresh,bw=bw,bh=bh,mask=mask)


def main():
    for arg in sys.argv[1:]:
        print(arg)

if __name__ == "__main__":

    path = "/Users/voltar/Desktop/N_5907/n5907_all/" # path the directory containing all images
    bias_names="bias*.fits" # how to identify the bias images by file name. * stands for wildcard ( all strings allowed)
    dark_names="dark600*.fits" # how to identify the dark images by file name.
    dark_for_flats_names="dark*5*.fits" # how to identify the bias images which are used on the flats by file name.
    flats_names="flat*L*.fits" # how to identify the flat field images by file name.
    science_names="ngc*wcs*.fits" # how to identify the science images by file name.
    # I did this naming convention because the telescope pipeline doesn't include astrometry, which therefore needed
    # to be done separately. To keep everything in one folder, I added "wcs" to the file name and search for it
    # in this name query. Of course there are smarter ways to do this, but it works.

    dark_to_science_ratio=2 # dark frames were taken with 600 seconds, but our exposures were rather 300s.
    # Assuming linearity, we can simply divide by two to get the right dark current.
    dark_flat_to_flat_ratio=1 # flat fields were 5 seconds, darks for flat also 5, so the ratio is 1.

    with_calib=True

    os.chdir(path)
    if with_calib==True:

        biases=[]
        for file in glob.glob(bias_names):
            biases.append(fits.open(file)[0].data)
        biases=np.array(biases)
        bias=np.median(biases,axis=0)

        #plt.imshow(bias)
        #plt.show()

        darks=[]
        for file in glob.glob(dark_names):
            darks.append(fits.open(file)[0].data)
        darks=np.array(darks)
        dark=np.median(darks,axis=0)
        dark=dark/dark_to_science_ratio

        darks_flat=[]
        for file in glob.glob(dark_for_flats_names):
            darks_flat.append(fits.open(file)[0].data-bias)
        darks_flat=np.array(darks)
        dark_flat=np.median(darks,axis=0)
        dark_flat = dark_flat / dark_flat_to_flat_ratio

        #plt.imshow(dark)
        #plt.show()


        flats=[]
        for file in glob.glob(flats_names):
            flat_db=fits.open(file)[0].data-dark_flat
            flat_N=flat_db/np.median(flat_db)
            flats.append(flat_N)
        flats=np.array(flats)
        flat=np.median(flats,axis=0)

        # in the following we estimate the background
        science_for_bg=[]
        for file in glob.glob(science_names):
            hdu= fits.open(file)[0]
            # calibrate the data
            science_db=hdu.data-dark-bias
            science=science_db/flat
            science=science/np.median(science)

            # now get the segmentation map for the mask
            img = science.byteswap().newbyteorder()
            objects, data_sub, bkg, globalrms, segmap = source_detection(img, thresh=1, minarea=5)
            # now dilate the mask to catch the faint outskirts source extractor didn't get with a gaussian filter
            segmap=np.nan_to_num(segmap)
            segmap[segmap > 0.0]=100
            segmap[segmap <= 0.0] = 0
            segmap_gauss=gaussian_filter(segmap,sigma=5)

            # now mask the data
            data_masked = np.ma.masked_where(segmap_gauss >0, img,True)
            # replace the masked pixels with nan such that in the stacking later it will be ignored
            img[data_masked.mask == True] = np.nan
            science_for_bg.append(img)

        # here we stack all individual masked images containing the background.
        # np.nanmedian ignores nan values in the median stack.
        # This will produce a background model.
        bg_from_images = np.nanmedian(science_for_bg, axis=0)


        # now we can properly reduce the science frames with our backgound model
        sciences=[]
        hdu_list=[]
        for file in glob.glob(science_names):
            hdu= fits.open(file)[0]

            # calibrate the data
            plt.imshow(hdu.data)
            science_db=hdu.data-dark-bias
            science=science_db/flat

            # run source extractor to get an estimate of the
            img = science.copy().byteswap().newbyteorder()
            objects_sc, data_sub_sc, bkg_sc, globalrms_sc, segmap_sc = source_detection(img, thresh=1, minarea=5)

            image_sub=np.subtract(science, np.multiply(bg_from_images.data,bkg_sc.globalback))

            hdu.data=image_sub
            hdu_list.append(hdu)

            wcs = WCS(hdu.header)
            hdu.header.update(wcs.to_header())
            hdu.writeto(path+"calibrated_" + file, overwrite=True)
